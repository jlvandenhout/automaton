## Automaton

This package provides a simple automaton implementation for Python.

---
- [1. Installation](#1-installation)
  - [1.1. Using pip](#11-using-pip)
  - [1.2. From source](#12-from-source)
- [2. Basic usage](#2-basic-usage)
- [3. Support](#3-support)
- [4. Contributing](#4-contributing)
- [5. License](#5-license)
- [6. Changelog](#6-changelog)
---

### 1. Installation
#### 1.1. Using pip
Simply run the usual installation command for pip:

```
pip install jlvandenhout-automaton
```

#### 1.2. From source
To install from the latest source code, clone this repository and install from the repository:

```
git clone https://gitlab.com/jlvandenhout/automaton.git
cd automaton
pip install .
```

### 2. Basic usage
```python
from random import choices

from jlvandenhout.automaton import Automaton
from jlvandenhout.graph import Graph


graph = Graph()
graph.edges.set("ice", "water", "heat")
graph.edges.set("water", "vapor", "heat")
graph.edges.set("vapor", "vapor", "heat")
graph.edges.set("vapor", "water", "cool")
graph.edges.set("water", "ice", "cool")
graph.edges.set("ice", "ice", "cool")

def follow(edge, symbol=None):
    return symbol == edge.get()

automaton = Automaton(graph, choices(list(graph.nodes)), graph.nodes, follow)

print("We start with", automaton.nodes.pop())
for symbol in choices(list(automaton.symbols), k=10):
    automaton.update(symbol)
    print(f"After {symbol}ing we have", automaton.nodes.pop())
```

### 3. Support
If you have any questions, suggestions or found a bug, please open an issue in [the issue tracker](https://gitlab.com/jlvandenhout/automaton/issues).

### 4. Contributing
Refer to [CONTRIBUTING](https://gitlab.com/jlvandenhout/automaton/blob/master/CONTRIBUTING.md).

### 5. License
Refer to [GNU General Public License v3 (GPLv3)](https://choosealicense.com/licenses/gpl-3.0/).

### 6. Changelog
Refer to [CHANGELOG](https://gitlab.com/jlvandenhout/automaton/blob/master/CHANGELOG.md).
