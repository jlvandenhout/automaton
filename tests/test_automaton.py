import pytest

from jlvandenhout.automaton import Automaton
from jlvandenhout.graph import Graph


def test_update():
    graph = Graph()
    graph.edges.set(0, 1, "a")
    graph.edges.set(1, 2, "b")
    graph.edges.set(2, 0)

    def follow(edge, symbol=None):
        value = edge.get()
        if value is None:
            return None
        if symbol:
            return symbol in value
        return False

    automaton = Automaton(graph, {0}, {2}, follow)
    assert {0} == automaton.nodes

    automaton.update("a")
    assert {1} == automaton.nodes

    automaton.update("b")
    assert {0, 2} == automaton.nodes


def test_match_simple_pattern():
    graph = Graph()
    graph.edges.set(0, 1, "a")
    graph.edges.set(1, 2, "b")
    graph.edges.set(2, 0)

    def follow(edge, symbol=None):
        value = edge.get()
        if value is None:
            return None
        if symbol:
            return symbol in value
        return False

    automaton = Automaton(graph, {0}, {2}, follow)
    assert tuple(automaton.consume("abab")) == (set(), set(), {2}, set(), {2})

    automaton.nodes = {0}
    assert tuple(automaton.consume("abba")) == (set(), set(), {2})
