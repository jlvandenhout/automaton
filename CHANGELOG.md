## Changelog
All usage related changes to this project will be logged in this file.

> Based on [Keep a Changelog](https://keepachangelog.com) and [Semantic Versioning](https://semver.org).

### [0.1.0] - 2019-08-13
#### Added
- An automaton implementation based on [graphs](https://gitlab.com/jlvandenhout/graph).
- CHANGELOG, CONTRIBUTING, LICENSE and README.